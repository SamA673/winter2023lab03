import java.util.Scanner; 

public class NationalPark{
	public static void main(String[] args){
			Scanner scan = new Scanner(System.in);
			
			// A group of sloths is called a "bed"! 
			Sloth[] Bed = new Sloth[4]; 
			
			// Loop through Bed Array slots and populate with user input!
			for (int i = 0; i<Bed.length; i++){
				// Initialize Sloth object and put it in Bed array
				Bed[i] = new Sloth(); 
				// Sloth name 
				System.out.println("What is Sloth #" + (i+1) + "'s name?"); 
				Bed[i].Name = scan.nextLine(); 
				// Sloth age 
				System.out.println("How old is " + Bed[i].Name + "?"); 
				Bed[i].Age = Integer.parseInt(scan.nextLine()); 
				// Sloth weight
				System.out.println("How many pounds does " + Bed[i].Name + " weigh?"); 
				Bed[i].Weight = Double.parseDouble(scan.nextLine()); 
				System.out.println("*************************************************"); 
			} 
			
			// Seperator 
			System.out.println();
			System.out.println("_________________________________________________");
			System.out.println();
			
			// Last Sloth's detail 
			System.out.println("Sloth " + Bed.length + "'s details!"); 
			System.out.println("Name:   " + Bed[(Bed.length -1)].Name); 
			System.out.println("Weight: " + Bed[(Bed.length -1)].Weight + " lbs"); 
			System.out.println("Age:    " + Bed[(Bed.length -1)].Age + "year(s) old"); 
		
			// Seperator 
			System.out.println();
			System.out.println("_________________________________________________");
			System.out.println();
			
			// First Sloth activity 
			System.out.println("Looks like " + Bed[0].Name + " is up to something!"); 
			System.out.println();
			
			Bed[0].eatLeaves(); 
			Bed[0].climbTrees(); 
		
		
	} 



} 