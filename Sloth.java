public class Sloth{
	public int Age; 
	public double Weight; 
	public String Name; 
	
	
	public void eatLeaves(){
		double oldWeight = this.Weight; 
		this.Weight += 0.2; 
		System.out.println( this.Name + " just ate a bunch of leaves and went from " + oldWeight + " lbs to " + this.Weight + " lbs!" ); 
		
	}

	
	public void climbTrees(){
		System.out.println(this.Name + " just climbed a tree!" ); 
		
	}
	
	
}

